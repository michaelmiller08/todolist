﻿using System;
using System.Collections.Generic;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public interface ITaskRepository
    {
        void AddTask(TaskModel task);

        void DeleteTask(Guid id);

        IEnumerable<TaskModel> GetAllTasks();
    }
}

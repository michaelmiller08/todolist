﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        readonly IList<TaskModel> _tasks;

        public TaskRepository()
        {
            if (_tasks == null)
            {
                _tasks = new List<TaskModel>();
            }
        }

        public void AddTask(TaskModel task)
        {
            if (task != null)
            {
                _tasks.Add(task);
            }
        }

        public void DeleteTask(Guid id)
        {
            var task = _tasks.SingleOrDefault(x => x.Id == id);
            _tasks.Remove(task);
        }

        public IEnumerable<TaskModel> GetAllTasks()
        {
            return _tasks;
        }
    }
}

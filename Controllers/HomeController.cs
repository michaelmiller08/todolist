﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Models;
using ToDoList.Services;
using ToDoList.ViewModels;

namespace ToDoList.Controllers
{
    public class HomeController : Controller
    {
        readonly ITaskService _taskService;

        public HomeController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        public IActionResult Index(IndexViewModel indexViewModel)
        {
            indexViewModel.Tasks = _taskService.GetAllTasks().ToList();
            return View(indexViewModel);
        }

        public IActionResult AddTask(IndexViewModel indexViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var task = indexViewModel?.Task;

            if (indexViewModel != null && !string.IsNullOrEmpty(task.Description))
            {
                task.Id = Guid.NewGuid();
                _taskService.AddTask(task);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult Edit(Guid? id, bool value)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }

            var task = _taskService.GetAllTasks().FirstOrDefault(x => x.Id == id);
            task.IsCompleted = value;

            var viewmodel = new IndexViewModel
            {
                Task = task
            };

            return RedirectToAction("Index", "Home", task);
        }

        public IActionResult DeleteTask(Guid id)
        {
            _taskService.DeleteTask(id);

            return RedirectToAction("Index", "Home");
        }
    }
}

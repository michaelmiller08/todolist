﻿using System;
namespace ToDoList.Models
{
    public class TaskModel
    {
        public string Description { get; set; }

        public bool IsCompleted { get; set; }

        public Guid Id { get; set; }
    }
}

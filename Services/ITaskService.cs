﻿using System;
using System.Collections.Generic;
using ToDoList.Models;

namespace ToDoList.Services
{
    public interface ITaskService
    {
        void AddTask(TaskModel task);

        void DeleteTask(Guid id);

        IEnumerable<TaskModel> GetAllTasks();
    }
}

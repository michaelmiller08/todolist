﻿using System;
using System.Collections.Generic;
using ToDoList.Models;
using ToDoList.Repositories;

namespace ToDoList.Services
{
    public class TaskService : ITaskService
    {
        readonly ITaskRepository _taskRepository;

        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public void AddTask(TaskModel task)
        {
            _taskRepository.AddTask(task);
        }

        public void DeleteTask(Guid id)
        {
            _taskRepository.DeleteTask(id);
        }

        public IEnumerable<TaskModel> GetAllTasks()
        {
            return _taskRepository.GetAllTasks();
        }
    }
}

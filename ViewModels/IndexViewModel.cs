﻿using System;
using System.Collections.Generic;
using ToDoList.Models;

namespace ToDoList.ViewModels
{
    public class IndexViewModel
    {
        public TaskModel Task { get; set; }

        public IList<TaskModel> Tasks { get; set; }
    }
}
